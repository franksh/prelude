
class jsdict(dict):
  """:class:`jsdict` is a :class:`dict` which functions more like a JavaScript
  object, i.e. it exposes its keys as attributes.

  It still has the regular :class:`dict` methods.

  The following are exactly the same:

  >>> d = js_dict()
  >>> d['hello'] = 1
  >>> d.hello
  1
  >>> d.foobar = 2
  >>> d.get('foobar')
  2
  >>> setattr(d, 'X', 3)
  >>> d['X'] + d.X
  6
  >>> del d.X
  >>> 'X' in d
  False
  >>> del d['foobar']
  >>> hasattr(d, 'foobar')
  False

  Warning: setting a key with the same name as any of the standard
  :class:`dict` methods will effectively hide those methods.

  >>> d['items'] = lambda: "NO"
  >>> d.items()
  "NO"

  """
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__dict__ = self

class jsdict_default(jsdict):
  def __init__(self, func, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__attr_func = func
  def __getattr__(self, name):
    return self.__attr_func(name)

