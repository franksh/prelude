

def moving_linear_average(n, initial=None):
  lst = []
  def _add(v):
    lst.append(v)
    if len(lst) > n:
      lst.pop(0)
    return sum(lst)/len(lst)
  if initial is not None:
    _add(initial)
  return _add

