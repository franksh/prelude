
"""
prelude.py contains some types and utilities that I consider missing from standard Python.
"""

class cached_property(object):
  """A non-data descriptor version of :class:`property()` which caches the computed value in the object's :attr:`__dict__`.

  This means you can use it like this:

  >>> obj = SomeObject()
  >>> obj.returntwo
  returntwo getter function called!
  2
  >>> obj.returntwo # no code will be executed: just like a regular attribute lookup
  2
  >>> clear_prop_cache(obj)
  >>> obj.returntwo
  returntwo getter function called!
  2

  It will add cached keys to the object's :attr:`_prop_cache` attribute (a
  :class:`set`), which will be instantiated if it doesn't exist.

  """
  def __init__(self, meth):
    self.__doc__ = getattr(meth, '__doc__', '')
    self._meth = meth
    self._name = meth.__name__

  def __get__(self, obj, cls):
    value = obj.__dict__[self._name] = self._meth(obj)
    if not hasattr(obj, '_prop_cache'):
      obj._prop_cache = set()
    obj._prop_cache.add(self._name)
    return value

  # Python quirk pop-quiz: what would happen if __set__() was defined to raise
  # an exception?
  #
  # [Learning the hard way: it would become a data-descriptor and override
  # the object's __data__ dictionary.]

def clear_prop_cache(obj):
  """Clears the property cache of an object that has :class:`@cached_property<cached_property>` decorators."""
  if not hasattr(obj, '_prop_cache'):
    return
  for p in obj._prop_cache:
    if p in obj.__dict__:
      del obj.__dict__[p]
  del obj._prop_cache

