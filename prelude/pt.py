

class Pt(tuple):
  def __new__(cls, *args):
    if len(args) > 1:
      return super().__new__(cls, args)
    else:
      return super().__new__(cls, *args)

  def add_pt(self, other):
    return Pt(a+b for (a,b) in zip(self, other))
  def add_val(self, val):
    return Pt(a+val for a in self)
  def sub_pt(self, other):
    return Pt(a-b for (a,b) in zip(self, other))
  def sub_val(self, val):
    return Pt(a-val for a in self)
  def rsub_pt(self, other):
    return Pt(b-a for (a,b) in zip(self, other))
  def rsub_val(self, val):
    return Pt(val-a for a in self)
  def mul_pt(self, other):
    return Pt(a*b for (a,b) in zip(self, other))
  def mul_val(self, val):
    return Pt(a*val for a in self)
  def div_pt(self, other):
    return Pt(a/b for (a,b) in zip(self, other))
  def div_val(self, val):
    return Pt(a/val for a in self)
  def rdiv_pt(self, other):
    return Pt(b/a for (a,b) in zip(self, other))
  def rdiv_val(self, val):
    return Pt(val/a for a in self)

  def __add__(self, o):
    return self.add_pt(o) if isinstance(o, tuple) else self.add_val(o)
  def __sub__(self, o):
    return self.sub_pt(o) if isinstance(o, tuple) else self.sub_val(o)
  def __rsub__(self, o):
    return self.rsub_pt(o) if isinstance(o, tuple) else self.rsub_val(o)
  def __mul__(self, o):
    return self.add_pt(o) if isinstance(o, tuple) else self.mul_val(o)
  def __div__(self, o):
    return self.div_pt(o) if isinstance(o, tuple) else self.div_val(o)
  def __rdiv__(self, o):
    return self.rdiv_pt(o) if isinstance(o, tuple) else self.rdiv_val(o)

  @property
  def x(self):
    return self[0]
  @property
  def y(self):
    return self[1]
  @property
  def z(self):
    return self[2]

  def mesh_points(self):
    return (Pt(x,y) for x in range(self.x) for y in range(self.y))
