
import argparse
import importlib
from pathlib import Path
import appdirs
from functools import wraps
import prelude

ARGS = prelude.jsdict()

class CliAction(argparse.Action):
  def __init__(self, *args, func=None, type=None, const=None, default=None, required=None, nargs=None, **kwargs):
    if func is None:
      func = lambda x: x
    if type is bool:
      if default is None:
        default = False
      const = not default
      super().__init__(*args, const=const, default=default, required=False, nargs=0, **kwargs)
      self.func = lambda x: func(const)
    else:
      super().__init__(*args, type=type, const=const, default=default, required=required, nargs=nargs, **kwargs)
      self.func = func
  def __call__(self, parser, namespace, values, option_string=None):
    setattr(namespace, self.dest, self.func(values))

def cli_decorator(*args, **kwargs):
  return lambda fn: option(*args, func=fn, **kwargs)

def arg(name, typ, default=None, func=None, **kwargs):
  if name.startswith('<') and name.endswith('>'):
    name = name[1:-1]
  return lambda p: p.add_argument(name, type=typ, func=func, default=default, action=CliAction, **kwargs)

def option(name, typ, default=None, func=None, required=False, **kwargs):
  args = (f'--{name}',)
  if '|' in name:
    l,s =  name.split('|')
    args = (f'--{l}', f'-{s}')
  return lambda p: p.add_argument(*args, type=typ, func=func, required=required, default=default, action=CliAction, **kwargs)

def with_module(mod, fun):
  import importlib
  try:
    return fun(importlib.import_module(mod))
  except ImportError:
    pass

opt_verbose = option("verbose|v", bool)

@cli_decorator("seed|s", int, None)
def opt_seed(seed):
  import random
  if seed == -1:
    seed = random.getrandbits(32)
  random.seed(seed)
  with_module('numpy', lambda np: np.random.seed(seed))
  with_module('torch', lambda t: t.manual_seed(seed))
  return seed

@cli_decorator("config|c", Path, None)
def opt_config(conf):
  from prelude import load_yaml
  if not conf.is_file():
    raise FileNotFoundError(f'config file {conf} is does not exist or is not a file')
  return load_yaml(conf.open('r'))


def git_style(base_flags, *commands, **kwargs):
  import sys

  p = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, **kwargs)
  p.set_defaults(_callback=lambda x:p.print_help())
  subs = p.add_subparsers()

  for cmd in commands:
    sub = subs.add_parser(cmd[1], help=cmd[2], formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    sub.set_defaults(_callback=cmd[0])
    for op in base_flags:
      op(sub)
    for op in cmd[3:]:
      op(sub)

  p.parse_args(sys.argv[1:], ARGS)
  return ARGS._callback(ARGS)

