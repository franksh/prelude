
import sys

"""
Import this file to automatically install a `pdb` hook that triggres on `AssertionError`s.
"""

def _info_hook(type, value, tb):
  """This is stolen from some StackOverflow or other. Can't remember where."""
  if hasattr(sys, 'ps1') or not sys.stderr.isatty() or type not in [AssertionError, ValueError, IndexError]:
    # Use default hook.
    sys.__excepthook__(type, value, tb)
  else:
    import traceback, pdb
    traceback.print_exception(type, value, tb)
    print
    # Start pdb interaction.
    pdb.pm()

sys.excepthook = _info_hook
