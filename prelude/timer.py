
import time

class simple_timer(object):
  @staticmethod
  def time(func, *args):
    start = time.time()
    v = func(*args)
    return time.time() - start, v

  def __init__(self):
    self._last = time.time()
  def __call__(self, func=None):
    res = time.time() - self._last
    self._last += res
    return res

  def has_passed(self, t):
    res = time.time() - self._last
    if res < t:
      return False
    self._last += res
    return res
