
import yaml

from .jsdict import jsdict


def load_yaml(stream, Loader=yaml.Loader, dict_like=jsdict):
  class CustomLoad(Loader):
    pass
  def construct_mapping(loader, node):
    loader.flatten_mapping(node)
    return dict_like(loader.construct_pairs(node))
  CustomLoad.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, construct_mapping)
  return yaml.load(stream, CustomLoad)

def dump_yaml(data, stream=None, Dumper=yaml.Dumper, **kwargs):
  class CustomDumper(Dumper):
    pass
  def _dict_representer(dumper, data):
    return dumper.represent_mapping(
      yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
      data.items())
  CustomDumper.add_representer(jsdict, _dict_representer)
  return yaml.dump(data, stream, CustomDumper, **kwargs)

